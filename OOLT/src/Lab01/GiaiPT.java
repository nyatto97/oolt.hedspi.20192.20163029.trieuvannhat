package Lab01;

import javax.swing.JOptionPane;

public class GiaiPT {

	public static void main(String[] args) {
		String strA, strB, strC;
		double A, B, C;
		
		strA = JOptionPane.showInputDialog(null, "Nhap he so A = ", "A", JOptionPane.INFORMATION_MESSAGE);
		strB = JOptionPane.showInputDialog(null, "Nhap he so B = ", "B", JOptionPane.INFORMATION_MESSAGE);
		strC = JOptionPane.showInputDialog(null, "Nhap he so C = ", "C", JOptionPane.INFORMATION_MESSAGE);
		A = Double.parseDouble(strA);
		B = Double.parseDouble(strB);
		C = Double.parseDouble(strC);
		giai(A, B, C);

	}
	
	public static void giai(double a, double b, double c) {
		double x1, x2;
		String nghiem;
		//Kiem tra cac he so
		if(a==0) {
			if(b==0) {
				nghiem = "Phuong trinh vo nghiem";
				JOptionPane.showMessageDialog(null, nghiem, "KET QUA", JOptionPane.INFORMATION_MESSAGE);
			}else {
				nghiem = "Phuong trinh co mot nghiem: " + "x = " + (-c/b);
				JOptionPane.showMessageDialog(null, nghiem, "KET QUA", JOptionPane.INFORMATION_MESSAGE);
			}
			return;
		}
		//Tinh delta
		double delta = b*b - 4*a*c;
		//Tinh nghiem
		if(delta > 0) {
			x1 = (double) ((-b + Math.sqrt(delta))/(2*a));
			x2 = (double) ((-b - Math.sqrt(delta))/(2*a));
			nghiem = "Phuong trinh co 2 nghiem la : x1 = " + x1 + " va x2 = " + x2;
			JOptionPane.showMessageDialog(null, nghiem, "KET QUA", JOptionPane.INFORMATION_MESSAGE);
		}else if(delta == 0) {
			x1 = (-b/(2*a));
			nghiem = "Phuong trinh co nghiem kep: x1 = x2 = " + x1;
			JOptionPane.showMessageDialog(null, nghiem, "KET QUA", JOptionPane.INFORMATION_MESSAGE);
		}else {
			nghiem = "Phuong trinh vo nghiem!";
			JOptionPane.showMessageDialog(null, nghiem, "KET QUA", JOptionPane.INFORMATION_MESSAGE);
		}
	}

}
