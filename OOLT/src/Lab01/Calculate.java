package Lab01;

import javax.swing.JOptionPane;

public class Calculate {

	public static void main(String[] args) {
		String strNum1, strNum2;
		double num1, num2, sum, difference, product, quotient;
		
		String strNotification = "";
		strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
		num1 = Double.parseDouble(strNum1);
		strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
		num2 = Double.parseDouble(strNum2);
		
		sum = num1 + num2;
		strNotification += num1 + " + " + num2 + " = " + sum + "\n";
		difference = num1 - num2;
		strNotification += num1 + " - " + num2 + " = " + difference + "\n";
		product = num1 * num2;
		strNotification += num1 + " * " + num2 + " = " + product + "\n";
		quotient = num1/num2;
		strNotification += num1 + " / " + num2 + " = " + quotient + "\n";

		JOptionPane.showMessageDialog(null, strNotification, "Calculate", JOptionPane.INFORMATION_MESSAGE);

	}

}
