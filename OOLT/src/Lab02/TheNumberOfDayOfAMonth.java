package Lab02;

import java.util.Scanner;;
public class TheNumberOfDayOfAMonth {

	public static void main(String[] args) {
		try (Scanner key = new Scanner(System.in)) {
			int Day=0, Month, Year;
			
			System.out.println("Please input month: ");
			Month = key.nextInt();
			while(Month < 1 || Month > 12) {
				System.out.println("Please input Month from 1 to 12, try again: ");
				Month = key.nextInt();
			}
			
			System.out.println("Please input Year: ");
			Year = key.nextInt();
			while(Year < 0) {
				System.out.println("Please input Year > 0, try again: ");
				Year = key.nextInt();
			}
			
			switch(Month) {
				case 1:
					Day = 31;
					break;
				case 2:{
					if(((Year%4 == 0) && (Year%100 != 0)) || (Year%400 == 0)) {
						Day = 29;
					}else {
						Day = 28;
					}
					break;
				}
				case 3:
					Day = 31;
					break;
				case 4:
					Day = 30;
					break;
				case 5:
					Day = 31;
					break;
				case 6:
					Day = 30;
					break;
				case 7:
					Day = 31;
					break;
				case 8:
					Day = 31;
					break;
				case 9:
					Day = 30;
					break;
				case 10:
					Day = 31;
					break;
				case 11:
					Day = 30;
					break;
				case 12:
					Day = 31;
					break;
			}
			System.out.println("The number of days of month: " + Month + ", year: " + Year + " are: " + Day);
		}

	}

}
