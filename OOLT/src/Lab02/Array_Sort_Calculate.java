package Lab02;

import java.util.Scanner;
import java.util.Arrays;

public class Array_Sort_Calculate {

	public static void main(String[] args) {
		try (Scanner key = new Scanner(System.in)) {
			int n, i, k, tmp, sum = 0;
			float average;
			
			System.out.print("Nhap so phan tu cua mang: ");
			n = key.nextInt();
			int a[] = new int[n];
			System.out.println("Nhap cac phan tu cua mang: ");
			for(i = 0; i < n; i++) {
				a[i] = key.nextInt();
				sum += a[i];
			}
			average = (float)sum/n;
			System.out.println("Mang goc: " + Arrays.toString(a));
			for(i = 0; i < n; i++) {
				for(k = i+1; k < n; k++) {
					if(a[i] > a[k]) {
						tmp = a[i];
						a[i] = a[k];
						a[k] = tmp;
					}
				}
			}
			System.out.println("Sort array: " + Arrays.toString(a));
			System.out.println("Sum of array: " + sum);
			System.out.println("Average of array: " + average);
		}

	}

}
