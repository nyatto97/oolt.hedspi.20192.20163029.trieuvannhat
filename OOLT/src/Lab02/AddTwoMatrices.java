package Lab02;

import java.util.Scanner;
public class AddTwoMatrices {

	public static void main(String[] args) {
		try (Scanner Input = new Scanner(System.in)) {
			int rows, columns, i, j;
			System.out.print("Enter number of rows: ");
			rows = Input.nextInt();
			System.out.print("Enter number of columns: ");
			columns = Input.nextInt();

			int matrix1[][] = new int[rows][columns];
			int matrix2[][] = new int[rows][columns];
			int sum[][] = new int[rows][columns];

			System.out.println("Enter all elements of the first matrix: ");
			for (i = 0; i < rows; i++)
			    for(j = 0; j < columns; j++)
			        matrix1[i][j] = Input.nextInt();

			System.out.println("Enter all elements of the second matrix: ");
			for (i = 0; i < rows; i++)
			    for(j = 0; j < columns; j++)
			    {
			        matrix2[i][j] = Input.nextInt();
			        sum[i][j] = matrix1[i][j] + matrix2[i][j];
			    }

			System.out.println("The first matrix: ");
			for (i = 0; i < rows; i++)
			{
			    for(j = 0; j < columns; j++)
			        System.out.print(matrix1[i][j] + "\t");
			    System.out.println();
			}

			System.out.println("The second matrix: ");
			for (i = 0; i < rows; i++)
			{
			    for(j = 0; j < columns; j++)
			        System.out.print(matrix2[i][j] + "\t");
			    System.out.println();
			}

			System.out.println("The sum of two matrices: ");
			for (i = 0; i < rows; i++)
			{
			    for(j = 0; j < columns; j++)
			        System.out.print(sum[i][j] + "\t");
			    System.out.println();
			}
		}

	}

}
